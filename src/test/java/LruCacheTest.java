import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class LruCacheTest {

    private final int CACHE_SIZE = 5;

    @Test
    public void test() {
        Map<Integer, String> data = new HashMap<>();
        data.put(0, "text");
        data.put(1, "text1");
        data.put(2, "text2");
        data.put(3, "text3");
        data.put(4, "text4");

        Cache<Integer, String> cache = new LruCache<>(CACHE_SIZE);

        data.forEach(cache::put);

        assertEquals(data, cache.getAllData());
        assertEquals(CACHE_SIZE, cache.getAllData().size());
        for (int i = 0; i < CACHE_SIZE; i++) {
            assertTrue(cache.contains(i));
        }

        data.put(5, "text5");
        cache.put(5, "text5");

        assertNotEquals(data, cache.getAllData());
        assertEquals(CACHE_SIZE, cache.getAllData().size());
        for (int i = 1; i < 6; i++) {
            assertTrue(cache.contains(i));
        }

        data.put(3, "newText3");
        cache.put(3, "newText3");
        data.put(1, "newText1");
        cache.put(1, "newText1");
        data.put(5, "newText5");
        cache.put(5, "newText5");

        assertNotEquals(data, cache.getAllData());
        assertEquals(CACHE_SIZE, cache.getAllData().size());
        for (int i = 1; i < 6; i++) {
            assertTrue(cache.contains(i));
        }

        data.put(6, "text6");
        cache.put(6, "text6");

        assertNotEquals(data, cache.getAllData());
        assertEquals(CACHE_SIZE, cache.getAllData().size());
        for (int i = 1; i < 6; i++) {
            if (i == 2) {
                assertFalse(cache.contains(i));
                continue;
            }
            assertTrue(cache.contains(i));
        }

        cache.get(4);
        cache.get(4);

        assertNotEquals(data, cache.getAllData());
        assertEquals(CACHE_SIZE, cache.getAllData().size());
        for (int i = 1; i < 6; i++) {
            if (i == 2) {
                assertFalse(cache.contains(i));
                continue;
            }
            assertTrue(cache.contains(i));
        }

        data.put(7, "text7");
        cache.put(7, "text7");

        assertNotEquals(data, cache.getAllData());
        assertEquals(CACHE_SIZE, cache.getAllData().size());
        for (int i = 1; i < 6; i++) {
            if (i == 2 || i == 3) {
                assertFalse(cache.contains(i));
                continue;
            }
            assertTrue(cache.contains(i));
        }

        data.remove(0);
        data.remove(2);
        data.remove(3);

        assertEquals(data, cache.getAllData());
    }
}