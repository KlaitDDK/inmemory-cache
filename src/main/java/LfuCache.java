import java.util.*;

public class LfuCache<K, V> implements Cache<K, V> {
    private final int size;
    private final Map<K, V> cache;
    private final Map<K, Integer> keysFrequency;
    private final Map<Integer, LinkedHashSet<K>> frequencyKeys;
    private int minFrequency = 0;

    public LfuCache(int size) {
        if (size <= 0) {
            throw new IllegalArgumentException("Cache size must be greater than 0");
        }
        this.size = size;
        cache = new HashMap<>();
        keysFrequency = new HashMap<>();
        frequencyKeys = new HashMap<>();
        frequencyKeys.put(0, new LinkedHashSet<>());
    }

    @Override
    public Optional<V> get(K key) {
        if (!cache.containsKey(key)) {
            return Optional.empty();
        }
        increaseUsageFrequency(key);
        return Optional.ofNullable(cache.get(key));
    }

    @Override
    public void put(K key, V value) {
        if (cache.containsKey(key)) {
            cache.put(key, value);
            increaseUsageFrequency(key);
            return;
        }
        if (cache.size() >= size) {
            Set<K> keysWithLeastFrequency = frequencyKeys.get(minFrequency);
            K keyWithLeastFrequency = keysWithLeastFrequency.iterator().next();
            keysWithLeastFrequency.remove(keyWithLeastFrequency);
            keysFrequency.remove(keyWithLeastFrequency);
            cache.remove(keyWithLeastFrequency);
        }
        cache.put(key, value);
        keysFrequency.put(key, 0);
        frequencyKeys.get(0).add(key);
        minFrequency = 0;
    }

    @Override
    public Map<K, V> getAllData() {
        return new HashMap<>(cache);
    }

    @Override
    public boolean contains(K key) {
        return cache.containsKey(key);
    }

    private void increaseUsageFrequency(K key) {
        Integer frequency = keysFrequency.get(key);
        Integer newFrequency = frequency + 1;
        keysFrequency.put(key, frequency + 1);
        frequencyKeys.get(frequency).remove(key);
        if (!frequencyKeys.containsKey(newFrequency)) {
            frequencyKeys.put(newFrequency, new LinkedHashSet<>());
        }
        frequencyKeys.get(newFrequency).add(key);
        if (frequency == minFrequency && frequencyKeys.get(frequency).isEmpty()) {
            minFrequency++;
        }
    }
}
