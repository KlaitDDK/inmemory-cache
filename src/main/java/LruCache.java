import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

public class LruCache<K, V> implements Cache<K, V> {
    private final int size;
    private final Map<K, V> cache;

    public LruCache(int size) {
        if (size <= 0) {
            throw new IllegalArgumentException("Cache size must be greater than 0");
        }
        this.size = size;
        cache = new LinkedHashMap<>();
    }

    @Override
    public Optional<V> get(K key) {
        Optional<V> value = Optional.ofNullable(cache.get(key));
        value.ifPresent(v -> putLast(key, v));
        return value;
    }

    @Override
    public void put(K key, V value) {
        if (cache.containsKey(key)) {
            putLast(key, value);
            return;
        }
        if (size <= cache.size()) {
            K latestUsedKey = cache.keySet().iterator().next();
            cache.remove(latestUsedKey);
        }
        cache.put(key, value);
    }

    @Override
    public Map<K, V> getAllData() {
        return new LinkedHashMap<>(cache);
    }

    @Override
    public boolean contains(K key) {
        return cache.containsKey(key);
    }

    private void putLast(K key, V value) {
        cache.remove(key);
        cache.put(key, value);
    }
}
